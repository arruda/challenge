# -*- coding: utf-8 -*-


def some_function(x=0):
    import random

    y = random.randint(0, 15)
    if y == 0:
        raise Exception("Some very unclear exception message")
    return some_function(y)
