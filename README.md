# Take a guess
A small challenge, part of my blog post: [My Post](url)

## How to play?
run the program with:
`run.sh`
it will generate the `secret_function.pyc`, and rename the source to `secret_function_source.py`.
Then it will run `crazy_stuff.py`

This program will give some `very unclear exception at some point` when calling `some_function`.
This function is a recursive funcion that takes only the argument `x`.

### Objective
Your challenge is to discover the value of the `x` argument of the function `some_function` when it raised the exception.
Ex:
if `some_function` raised the exception in it's n° recursion, with `x` argument been equals to `10`, then the answer is `x=10`

## Rules
* Don't look at `secret_function_source.py`.
* You can change whatever you want in the `crazy_stuff.py` file, except the `some_function(x=1)` part.

# Submit you answer
Make a pull request for this repository with your solution =)
